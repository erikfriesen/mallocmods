#include <stddef.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "umm_malloc.h"
#include "xmem.h"
#include <sys/kmem.h>
#include "logs.h"
#include <stdio.h>
#include "system/devcon/sys_devcon.h"


extern void * __real_malloc(size_t);
extern void __real_free(void *);
extern void * __real_realloc(void *, size_t);
extern void * __real_calloc(size_t, size_t);

#define DDR_HEAP_ADDR (0x88000000)//cached space
#define DDR_HEAP_SIZE (32 * 1024 *1024)

#define HEAP1_CFG_HEAP_SIZE 0x48000

static unsigned char heap_array1[HEAP1_CFG_HEAP_SIZE] __attribute__((aligned(16)));
//Heap1 for general use
_Heap Heap1 = {.umm_heap = (umm_block*) heap_array1, .heap_size = HEAP1_CFG_HEAP_SIZE, .umm_numblocks = 0, .HeapReady = 0};
_Heap HeapDdr = {.umm_heap = (umm_block*) DDR_HEAP_ADDR, .heap_size = DDR_HEAP_SIZE, .umm_numblocks = 0, .HeapReady = 0};

void xmem_init(void) {
    umm_init(&Heap1);
    umm_init(&HeapDdr);
}

void * __wrap_malloc(int xWantedSize) {
    return x_malloc(xWantedSize, XMEM_HEAP_1);
}

void __wrap_free(void *ptr) {
    x_free(ptr);
}

void * __wrap_realloc(void * ptr, size_t size) {
    return x_realloc(ptr, size);
}

void * __wrap_calloc(size_t num, size_t size) {
    return x_calloc(num, size, XMEM_HEAP_1);
}

void * x_malloc(uint32_t size, uint32_t flags) {
    void *pvReturn = NULL;
    if (flags & (XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16)) {
	size += 8;
    }
    if (flags & XMEM_HEAP_DDR) {
	pvReturn = umm_malloc(&HeapDdr, size);
    } else {//XMEM_HEAP_1
	pvReturn = umm_malloc(&Heap1, size);
	if (pvReturn == NULL) {
	    pvReturn = umm_malloc(&HeapDdr, size);
	}
    }
    if (pvReturn) {
	if (flags & (XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16)) {
	    pvReturn = (void*) ((unsigned char*) pvReturn) + 8;
	    if (flags & (XMEM_HEAP_COHERENT)) {
		//Clear cache here so we don't get writebacks and some random time
		SYS_DEVCON_DataCacheClean((uint32_t)pvReturn, size); //Write down
		pvReturn = KVA0_TO_KVA1(pvReturn);
	    }
	}
    }
#if( configUSE_MALLOC_FAILED_HOOK == 1 )
    {
	if (pvReturn == NULL) {
	    extern void vApplicationMallocFailedHook(void);
	    vApplicationMallocFailedHook();
	}
    }
#endif
    return pvReturn;
}
void * x_calloc(size_t num, size_t size, uint32_t flags) {
    void *pvReturn = NULL;
    size_t Allocate = (size_t) (size * num);
    if (flags & (XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16)) {
	Allocate += 8;
    }
    if (flags & XMEM_HEAP_DDR) {
	pvReturn = umm_malloc(&HeapDdr, Allocate);
    } else {//XMEM_HEAP_1
	pvReturn = umm_malloc(&Heap1, Allocate);
	if (pvReturn == NULL) {
	    pvReturn = umm_malloc(&HeapDdr, Allocate);
	}
    }
    if (pvReturn) {
	memset(pvReturn, 0x00, (size_t) Allocate);
	if (flags & (XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16)) {
	    pvReturn = (void*) ((unsigned char*) pvReturn) + 8;
	    if (flags & (XMEM_HEAP_COHERENT)) {
		//Clear cache here so we don't get writebacks and some random time
		SYS_DEVCON_DataCacheClean((uint32_t) pvReturn, (size * num)); //Write down
		pvReturn = KVA0_TO_KVA1(pvReturn);
	    }
	}
    }

#if( configUSE_MALLOC_FAILED_HOOK == 1 )
    {
	if (pvReturn == NULL) {
	    extern void vApplicationMallocFailedHook(void);
	    vApplicationMallocFailedHook();
	}
    }
#endif
    return pvReturn;
}

void * x_realloc(void * ptr, size_t size) {
    void *pvReturn = NULL;
    int WasCoherent = 0;
    int WasAligned = 0;
    if (ptr && (uint32_t)ptr % 16 == 0) {
	//This is an aligned pointer, we need to downshift 8
	ptr = (void*) ((unsigned char*) ptr) - 8;
	WasAligned = 1;
    }
    if (IS_KVA1(ptr)) {
	WasCoherent = 1;
	ptr = KVA1_TO_KVA0(ptr);
    }
    if (ptr == NULL || ((unsigned char*) ptr >= (unsigned char*) Heap1.umm_heap && (unsigned char*) ptr < ((unsigned char*) Heap1.umm_heap) + Heap1.heap_size)) {
	pvReturn = umm_realloc(&Heap1, ptr, size);
    } else if ((unsigned char*) ptr >= (unsigned char*) HeapDdr.umm_heap && (unsigned char*) ptr < ((unsigned char*) HeapDdr.umm_heap) + HeapDdr.heap_size) {
	pvReturn = umm_realloc(&HeapDdr, ptr, size);
    }

#if( configUSE_MALLOC_FAILED_HOOK == 1 )
    {
	if (pvReturn == NULL) {
	    extern void vApplicationMallocFailedHook(void);
	    vApplicationMallocFailedHook();
	}
    }
#endif
    if (pvReturn && WasCoherent) {
	pvReturn = KVA0_TO_KVA1(pvReturn);
    }
    if (pvReturn && WasAligned) {
	pvReturn = (void*) ((unsigned char*) pvReturn) + 8;
    }
    return pvReturn;
}

void x_free(void * ptr) {
    if (ptr && (uint32_t)ptr % 16 == 0) {
	//This is an aligned pointer, we need to downshift 8
	ptr = (void*) ((unsigned char*) ptr) - 8;
    }
    if (IS_KVA1(ptr)) {
	ptr = KVA1_TO_KVA0(ptr);
    }
    if ((unsigned char*) ptr >= (unsigned char*) Heap1.umm_heap && (unsigned char*) ptr < ((unsigned char*) Heap1.umm_heap) + Heap1.heap_size) {
	umm_free(&Heap1, ptr);
    } else if ((unsigned char*) ptr >= (unsigned char*) HeapDdr.umm_heap && (unsigned char*) ptr < ((unsigned char*) HeapDdr.umm_heap) + HeapDdr.heap_size) {
	umm_free(&HeapDdr, ptr);
    }
}

void x_info(uint32_t heap, char *buff, int MaxLen) {
    switch (heap) {
	default:
	    snprintf(buff, MaxLen, "Unspecified Heap");
	    //lint -fallthrough
	case XMEM_HEAP_1:
	    (void)umm_info(&Heap1, 0, 1, buff, MaxLen);
	    break;
	case XMEM_HEAP_DDR:
	    (void)umm_info(&HeapDdr, 0, 1, buff, MaxLen);
	    break;
    }    
}

int x_integrity(uint32_t heap, char *buff, int MaxLen) {
    switch (heap) {
	default:
	    snprintf(buff, MaxLen, "Unspecified Heap");
	    return 0;
	case XMEM_HEAP_1:
	    return umm_integrity_check(&Heap1, buff, MaxLen);
	case XMEM_HEAP_DDR:
	    return umm_integrity_check(&HeapDdr, buff, MaxLen);
    }
}

void * x_malloc_uncached(size_t size) {
    return x_malloc(size, XMEM_HEAP_DDR | XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16);
}

void * x_calloc_uncached(size_t num, size_t size) {
    return x_calloc(num, size, XMEM_HEAP_DDR | XMEM_HEAP_COHERENT | XMEM_HEAP_ALIGN16);
}
